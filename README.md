FAST-PS CAENels Tango Device Server
==================================

Author(s): Roger Pastor Ortiz, [Sergi Blanch-Torne](sblanch@cells.es)

![license GPLv3+](https://img.shields.io/badge/license-GPLv3+-green.svg)
![4 - Beta](https://img.shields.io/badge/Development_Status-4_--_beta-orange.svg)

Description: Tango device server to control the FAST-PS of CAENels.

Current version: 1.0.12-alpha

Installation instructions:
-------------------------

The setup.cfg file contains an specific config
uration for @ ALBA:

    python setup.py install

For a regular installation:

    python setup.py install --no-user-cfg

There is also a bliss package (inside Alba) the build of which is in our [jenkins](http://jenkins.cells.es/job/Ds_FastPS/) infrastructure.

Prepare a test
--------------

From the source files,

    cd test
    python ds_create.py --machine=localhost --simulation --logpath=/tmp

This will generate 16 device servers (what is 1 per sector) with 2 devices each. The script will stay running as it is launching also 32 simulators of the hardware. The last parameter is necessary if the user of the test doesn't have write rights in the usual directory for the logs.

It can be also simply launched the simulator and one device server to work with this simulator:

    import CAENels
    simulator = CAENels.FastPS_PC(port=10002)
    simulator.open()

Device properties necessaries are 'HostIP' and 'Port'. The 'Logfile' property is optional in case one likes to establish an specific output directory for it.

