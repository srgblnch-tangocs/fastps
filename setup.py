# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

from distutils.core import setup

__author__ = "Roger Pastor Ortiz/Sergi Blanch-Torne"
__copyright__ = "Copyright 2015-2018, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"
__version__ = "1.0.12-alpha"

setup(
    name='FastPSCAEN',
    version=__version__,
    packages=['fastpscaen'],
    scripts=['script/FastPS', 'test/test_fastps_core.py'],
    options={'build_scripts': {'executable':
                                   '/usr/bin/env python',
                               },
             },
    license=__license__,
    author=__author__,
    author_email='sblanch@cells.es',
    description='Device server to control the FAST-PS',
    long_description='Tango Device Server to control the FAST-PS power supply'
                     'from CAENels.',
    release=1,
    # requires=[''],
    # conflicts=[''],
    classifiers=['Development Status :: 4 - Beta',
                 'Intended Audience :: Science/Research',
                 'License :: OSI Approved :: '
                 'GNU General Public License v3 or later (GPLv3+)',
                 'Programming Language :: Python',
                 'Topic :: Scientific/Engineering :: '
                 ''],
    url='https://git.cells.es/controls/fastps',
)
