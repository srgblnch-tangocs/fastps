# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

import socket
import logging
import logging.handlers
from math import isnan
from os import makedirs
import os.path
try:
    from tango import AttrQuality
except Exception:
    from PyTango import AttrQuality
from time import time
from threading import Thread, Event, Lock
import traceback

__author__ = "Roger Pastor Ortiz/Sergi Blanch-Torne"
__copyright__ = "Copyright 2015-2018, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

TCP_PORT = 10001

ERR_FASTPS_COMM_TIMEOUT = '50'
ERR_FASTPS_BAD_MODE = '51'
ERR_FASTPS_NO_PARAMS = '52'
ERR_FASTPS_INVALID_STATUS = '53'
ERR_FASTPS_WRONG_PARAMS = '54'
ERR_FASTPS_BAD_PARAMS = '55'

ATTR_QUALITY_INVALID = AttrQuality.ATTR_INVALID
ATTR_QUALITY_VALID = AttrQuality.ATTR_VALID
ATTR_QUALITY_CHANGING = AttrQuality.ATTR_CHANGING
ATTR_QUALITY_WARNING = AttrQuality.ATTR_WARNING
ATTR_QUALITY_ALARM = AttrQuality.ATTR_ALARM


def isclose(a, b, arel=1e-5, atol=1e-9):
    """
    In the old suse 11.1, the numpy doesn't have the isclose method to be
    directly imported. This is an implementation of what it may do (based only
    in floats to compare).
    :param a, b: objects that can be interpreted as floats
    :param arel:
    :param atol:
    :return:
    """
    def getFloat(x, excRaise=False):
        try:
            return float(x)
        except:
            if excRaise:
                raise ValueError("Could not convert {v} to float".format(v=x))
            else:
                return None

    a = getFloat(a)
    b = getFloat(b)
    arel = getFloat(arel, excRaise=True)
    atol = getFloat(atol, excRaise=True)
    if a is None or b is None or isnan(a) or isnan(b):
        return False
    return abs(a - b) <= max(arel * max(abs(a), abs(b)), atol)


class OutputMode(object):
    """
        Class to define and use as constants the possible mode output values
    """

    CURRENT = 'Current'
    VOLTAGE = 'Voltage'
    UNKNOWN = 'UNKNOWN'

    def __getitem__(self, key):
        """
        Method to check if the mode is correct
        :param key: input mode
        :return: value: if it is a possible one
        """
        mode = {
            1: self.VOLTAGE,
            0: self.CURRENT,
            'I': self.CURRENT,
            'V': self.VOLTAGE,
            'i': self.CURRENT,
            'v': self.VOLTAGE,

        }
        try:
            value = mode[key]
        except KeyError:
            value = self.UNKNOWN
        except Exception as e:
            raise RuntimeError('Unknown exception when retrieving function '
                               'mode: %s' % e)
        return value


class UpdateMode(object):
    """
        Class to define and use as constants the possible usage output values
    """
    NORMAL = 'NORMAL'
    WAVEFORM = 'WAVEFORM'
    FIFO = 'TRIGGERED FIFO'
    ANALOG = 'ANALOG INPUT'
    UNKNOWN = 'UNKNOWN'

    def __getitem__(self, key):
        """
        Method to check if the mode is correct
        :param key: input mode
        :return: value: if it is a possible one
        """
        mode = {
            3: self.ANALOG,
            2: self.FIFO,
            1: self.WAVEFORM,
            0: self.NORMAL,
        }
        try:
            value = mode[key]
        except KeyError:
            value = self.UNKNOWN
        except Exception as e:
            raise RuntimeError('Unknown exception when retrieving update '
                               'mode: %s' % e)
        return value


class ControlMode(object):
    """
        Class to define and use as constants the possible control mode values
    """
    REMOTE = 'REMOTE'
    LOCAL = 'LOCAL'
    UNKNOWN = 'UNKNOWN'
    FAST_INTERFACE = 'FAST_INTERFACE'
    NOT = 'Control mode not implemented'

    # TODO: Fast-Interface to be implemented once the firmware is updated
    def __getitem__(self, key):
        """
        Method to check if the mode is correct
        :param key: input mode
        :return: value: if it is a possible one
        """
        mode = {
            0: self.REMOTE,
            1: self.LOCAL,
            2: self.NOT,
            3: self.NOT,
        }
        try:
            value = mode[key]
        except KeyError:
            value = self.UNKNOWN
        except Exception as e:
            raise RuntimeError('Unknown exception when retrieving control '
                               'mode: %s' % e)
        return value


class StatusMode(object):
    """
        Class to define and use as constants the possible status mode values
    """
    ON = 'ON'
    OFF = 'OFF'
    FAULT = 'FAULT'
    UNKNOWN = 'UNKNOWN'

    def __getitem__(self, key):
        """
        Method to check if the mode is correct
        :param key: input mode
        :return: value: if it is a possible one
        """
        mode = {
            0: self.OFF,
            1: self.ON,
            2: self.FAULT,
            3: self.FAULT,
        }
        try:
            value = mode[key]
        except KeyError:
            value = self.UNKNOWN
        except Exception as e:
            raise RuntimeError('Unknown exception when retrieving status '
                               'mode: %s' % e)
        return value


class FaultConditions(object):
    """
        Class to define the dictionary of the possible fault conditions.
    """
    faultlist = {
      'ovc': ('Input OVC', 'Input Over Current'),
      'crowbar': ('Crowbar', 'Crowbar protection intervention'),
      'ovt': ('OVT', 'Over Temperature condition'),
      'dclink': ('DC-Link Fault', 'DC-Link under-voltage condition'),
      'earthleakage': ('Earth Leakage', 'Earth current leakage fault'),
      'earthfuse': ('Earth Fuse', 'Earth fuse is blown'),
      'regulation': ('Regulation Fault', 'Modules has experienced a '
                                         'regulation fault'),
      'ripple': ('Excessive Ripple', 'Module is having excessive ripple'),
      'interlock1': ('Ext. Interlock #1', 'External interlock 1 has '
                                          'tripped'),
      'interlock2': ('Ext. Interlock #2', 'External interlock 2 has '
                                          'tripped'),

    }


class FastPSError(Exception):
    """
        Class to define and return the possible errors of the Fast PS
    """

    def __init__(self, value, *args):
        """
        Method to acquire the error code value and possible args
        :param value: input error code
        :param args: other possible params.
        """
        self.errcode = value
        self.args = args

    def __str__(self):
        """
        Method to retrieve the error message to output
        :return message: Error code message
        """
        return self._get_error_msg(self.errcode)

    def _get_error_msg(self, value):
        """
        Method to parse the error code and retrieve its associated message.
        Done as if else structure and not in dict structure in behalf of read
        and edit easiness.
        :return msg: Error code message
        """
        if value == '01':
            msg = 'Error %s: Unknown command' % value
        elif value == '02':
            msg = 'Error %s: Unknown argument' % value
        elif value == '03':
            msg = 'Error %s: Index out of range' % value
        elif value == '04':
            msg = 'Error %s: Not Enough Arguments' % value
        elif value == '05':
            msg = 'Error %s: Privilege Level Requirement not met' % value
        elif value == '06':
            msg = 'Error %s: Saving Error on device' % value
        elif value == '07':
            msg = 'Error %s: Invalid password' % value
        elif value == '08':
            msg = 'Error %s: Power supply in fault' % value
        elif value == '09':
            msg = 'Error %s: Power supply already ON' % value
        elif value == '10':
            msg = 'Error %s: Setpoint is out of model limits' % value
        elif value == '11':
            msg = 'Error %s: Setpoint is out of software limits' % value
        elif value == '12':
            msg = 'Error %s: Setpoint is not a number' % value
        elif value == '13':
            msg = 'Error %s: Module is OFF' % value
        elif value == '14':
            msg = 'Error %s: Slew Rate out of limits' % value
        elif value == '15':
            msg = 'Error %s: Device is set in local mode' % value
        elif value == '16':
            msg = 'Error %s: Module is not in waveform mode' % value
        elif value == '17':
            msg = 'Error %s:Module is in waveform mode' % value
        elif value == '18':
            msg = 'Error %s: Device is set in remote mode' % value
        elif value == '19':
            msg = ('Error %s: Module is already in the selected loop mode'
                   % value)
        elif value == '20':
            msg = 'Error %s: Module is not in the selected loop mode' % value
        elif value == ERR_FASTPS_COMM_TIMEOUT:
            msg = ('Error %s: Error timed out, the device did not respond. '
                   % value)
        elif value == ERR_FASTPS_BAD_MODE:
            msg = ('Error %s: Error of loop mode, the device is not set to the'
                   ' correct mode. ' % value)
        elif value == ERR_FASTPS_NO_PARAMS:
            msg = ('Error %s: Error of parameters. Parameter expected, none '
                   'given' % value)
        elif value == ERR_FASTPS_WRONG_PARAMS:
            msg = ('Error %s: Error of parameters. Current or Voltage '
                   'expected, other given' % value)
        elif value == ERR_FASTPS_INVALID_STATUS:
            msg = 'Error %s: Error of Status. Status not known' % value
        elif value == ERR_FASTPS_BAD_PARAMS:
            msg = ('Error %s: Error of parameters. Parameter not writable '
                   'given' % value)
        else:
            msg = 'Unknown error (%s)' % value

        return msg


class StatusFastPS(object):
    """
        Class to define the Fast PS status.
    """

    name = None
    statusmode_obj = None
    controlmode_obj = None
    outputmode_obj = None
    updatemode_obj = None

    def __init__(self, name):
        """
        Method that sets the internal variables of the object
        :param fastps_register: parameter with the HW register of the power
        supply.
        """
        self.name = name
        self.statusmode_obj = StatusMode()
        self.controlmode_obj = ControlMode()
        self.outputmode_obj = OutputMode()
        self.updatemode_obj = UpdateMode()

    def __repr__(self):
        return "{name}(state={state}, ctrl={ctrl}, regulation={reg}, " \
               "ramp={ramp}, waveform={wf}, inputOverCurrent={ovc}, "\
               "crowbar={crow}, overTemperature={ovt}, dclink={dclink}"\
               "earthLeak={eleak}, earthFuse={efuse}, " \
               "regulationFault={regfault}, ripple={ripple}, " \
               "itlk1={itlk1}, itlk2={itlk2})".format(
            name=self.name, state=self.state, ctrl=self.controlmode,
            reg=self.regulationmode, ramp=self.ramping, wf=self.waveform,
            ovc=self.ovc, crow=self.crowbar, ovt=self.ovt, dclink=self.dclink,
            eleak=self.earthleakage, efuse=self.earthfuse,
            regfault=self.regulation, ripple=self.ripple,
            itlk1=self.interlock1, itlk2=self.interlock2)

    def flags(self):
        flags = []
        if self.ovc:
            flags.append("input over current")
        if self.crowbar:
            flags.append("crowbar protection intervention")
        if self.ovt:
            flags.append("over temperature")
        if self.dclink:
            flags.append("DC-link fault")
        if self.earthleakage:
            flags.append("earth current leakage")
        if self.earthfuse:
            flags.append("earth fuse is blown")
        if self.regulation:
            flags.append("regulation fault")
        if self.ripple:
            flags.append("excessive ripple")
        if self.interlock1:
            flags.append("Interlock 1 tripped")
        if self.interlock2:
            flags.append("Interlock 2 tripped")
        if self.ovp:
            flags.append("over power")
        if len(flags) > 0:
            return ", ".join(flags)
        return ""

    def update(self, fastps_register):
        self.raw_status = fastps_register
        # The bits 0 and 1 of the register status give the state of the PS if
        #  there is and error the default state is UNKNOWN
        self.state = self.statusmode_obj[self._get_bits(0, 2)]
        # Control Mode
        self.controlmode = self.controlmode_obj[(self._get_bits(2, 2))]
        # BIT 4 is reserved
        # Regulation Mode bit
        self.regulationmode = self.outputmode_obj[(self._get_bits(5, 1))]
        # Update Mode
        self.updatemode = self.updatemode_obj[(self._get_bits(6, 2))]
        # BITS 8-11 are reserved
        # Rampig current and waveform in execution
        self.ramping = self._is_set(12)
        self.waveform = self._is_set(13)
        # BITS 14-16 are reserved
        # Fault bits
        self.ovc = self._is_set(17)
        self.crowbar = self._is_set(18)
        # BITS 19 are reserved
        self.ovt = self._is_set(20)
        self.dclink = self._is_set(21)
        self.earthleakage = self._is_set(22)
        self.earthfuse = self._is_set(23)
        self.regulation = self._is_set(24)
        self.ripple = self._is_set(25)
        self.interlock1 = self._is_set(26)
        self.interlock2 = self._is_set(27)
        self.ovp = self._is_set(29)

    def _get_bits(self, start_bit, nbits=1):
        """
        Method that returns the value of the nbits from the status register,
        starting at the start_bit.
        :return bits
        """
        nbits -= 1
        value = self.raw_status >> start_bit
        mask = (1 << nbits+1) - 1
        return value & mask

    def _is_set(self,  n):
        """
        Method that checks if a bit is set
        :return bit: bool
        """
        return self.raw_status & 2**n != 0


class ROCommand(object):
    _name = None
    _cmd = None

    _rvalue = None
    _rvalue_change_cbs = None

    _timestamp = None
    _quality = ATTR_QUALITY_INVALID
    _close = None

    def __init__(self, name, cmd, parent=None, close=None, *args, **kwargs):
        super(ROCommand, self).__init__(*args, **kwargs)
        loggerName = 'FastPSLogger' if parent is None else parent._logger.name
        self._logger = logging.getLogger(loggerName)
        self._name = name
        if cmd is not None:
            self._cmd = cmd
        self._rvalue_change_cbs = []
        self._close = float(close) if close is not None else close

    @property
    def name(self):
        return self._name

    @property
    def command(self):
        return self._cmd

    @property
    def rvalue(self):
        if self._rvalue is not None:
            return self._rvalue
        elif hasattr(self, '_wvalue') and self._wvalue is not None:
            return self._wvalue
        return float('NaN')

    @rvalue.setter
    def rvalue(self, value):
        self._timestamp = time()
        if value is None:
            self._quality = ATTR_QUALITY_INVALID
            self._rvalue = value
        elif self._rvalue == value and self._quality != ATTR_QUALITY_VALID:
            self._quality = ATTR_QUALITY_VALID
        elif self._rvalue != value:
            if self._close is not None and \
                    isclose(self._rvalue, value, atol=self._close):
                self._quality = ATTR_QUALITY_VALID
            else:
                self._quality = ATTR_QUALITY_CHANGING
            self._rvalue = value
        else:
            return  # nothing to do if there os nothing new
        for cb in self._rvalue_change_cbs:
            if cb is not None:
                cb(self)

    @property
    def timestamp(self):
        return self._timestamp

    @property
    def quality(self):
        return self._quality

    def addRvalueChangeCB(self, cb):
        if self._rvalue_change_cbs.count(cb) > 0:
            raise AssertionError("Already exist")
        if self._rvalue_change_cbs.count(None) > 0:
            idx = self._rvalue_change_cbs.index(None)
            self._rvalue_change_cbs[idx] = cb
        else:
            self._rvalue_change_cbs.append(cb)
            idx = self._rvalue_change_cbs.index(cb)
        self._logger.debug("Added %d callback %s for %s"
                           % (idx, cb.__name__, self.name))
        return idx

    def removeRvalueChangeCB(self, idx):
        if len(self._rvalue_change_cbs) >= idx:
            if self._rvalue_change_cbs[idx] is not None:
                cb = self._rvalue_change_cbs[idx]
                self._rvalue_change_cbs[idx] = None
                self._logger.debug("Removed %d callback %s for %s"
                                   % (idx, cb.__name__, self.name))
                return True
        return False

    @property
    def wvalue(self):
        return None

    @wvalue.setter
    def wvalue(self, value):
        pass


class RWCommand(ROCommand):
    _wvalue = None

    def __init__(self, *args, **kwargs):
        super(RWCommand, self).__init__(*args, **kwargs)

    # TODO; write change callback

    @property
    def wvalue(self):
        if self._wvalue is not None:
            return self._wvalue
        return float('NaN')

    @wvalue.setter
    def wvalue(self, value):
        if value != self._wvalue:
            self._logger.debug("%s.wvalue change from %s to %s"
                               % (self._name, self._wvalue, value))
            self._wvalue = value


class RORegister(ROCommand):
    _regNumber = None

    def __init__(self, name, reg, *args, **kwargs):
        super(RORegister, self).__init__(name, cmd="MRG", *args, **kwargs)
        self._regNumber = reg

    @property
    def register(self):
        return self._regNumber

    @property
    def command(self):
        return "%s:%s" % (self._cmd, self._regNumber)


class RWRegister(RWCommand):
    _regNumber = None

    def __init__(self, name, reg, *args, **kwargs):
        super(RWRegister, self).__init__(name, cmd="MRG", *args, **kwargs)
        self._regNumber = reg

    @property
    def register(self):
        return self._regNumber

    @property
    def command(self):
        return "%s:%s" % (self._cmd, self._regNumber)


MAX_BUFF_SIZE_MODULE = 1024


class FastPS(object):
    """
    Class to control the FastPS Power supply.

    """
    _logfile = None
    _logger = None

    _ip = None
    _port = None
    _timeout = None
    _socket = None
    _semaphore = None
    _buff = None
    _reconnectFailed = None

    _outputmode = None
    _updatemode = None
    _controlmode = None
    _faultconditions = None
    _status = None

    _commands = None
    _loop = None
    _current = None
    _voltage = None
    _current_setpoint = None
    _voltage_setpoint = None
    _current_ramp = None
    _voltage_ramp = None
    _current_slew = None
    _voltage_slew = None
    _power = None
    _leakage_current = None
    _temperature = None
    _password = None

    # Internal dictionary to create dynamic attributes, the key is the name
    # of the attribute, the item is a RORegister or a RWRegister, depending
    # on if it is read only or protected, with the command number as a
    # constructor parameter
    _registers = None

    _updater = None
    _exitter = None
    _refresh_period = None

    _rvalue_change_cbs = None

    def __init__(self, ip, port=TCP_PORT, name='FastPSLogger', timeout=3,
                 logfile='/tmp/fast-ps.log', refresh=1.):
        """
        Method to initialise the object connecting to the FAST-PS and opening a
        log file
        :param ip: FastPS IP
        :param port: TCP_PORT
        :param timeout: of the socket
        :param logfile: path to the target log file
        :return: None
        """

        self._logfile = logfile
        # if the logfile goes throw a path with a directory that doesn't exist
        # create it.
        directories = logfile.split('/')[:-1]
        path = ""
        for directory in directories:
            path += "%s/" % directory
            if not os.path.isdir(path):
                makedirs(path)
        # Setup logging:

        # Set up a specific logger with our desired output level
        self._logger = logging.getLogger(name)
        self._logger.setLevel(logging.INFO)

        # Add the log message handler to the logger
        handler = logging.handlers.RotatingFileHandler(
            self._logfile, maxBytes=1024 * 1024 * 10, backupCount=5)
        formatter = logging.Formatter(
            '%(asctime)s %(levelname)-8s %(message)s')
        handler.setFormatter(formatter)
        self._logger.addHandler(handler)

        self._logger.info(
            "Initializing communications to {a}:{b} (timeout {c})".format(
                a=ip, b=port, c=timeout))

        self._ip = ip
        self._port = port
        self._timeout = timeout
        self._reconnectFailed = False
        self.link()

        self._status = StatusFastPS(name)
        self._outputmode = self._status.outputmode_obj
        self._updatemode = self._status.updatemode_obj
        self._controlmode = self._status.controlmode_obj
        self._faultconditions = FaultConditions()

        self._commands = {}
        for obj in [ROCommand("current", "MRI", parent=self, close=0.01),
                    ROCommand("voltage", "MRV", parent=self, close=0.01),
                    RWCommand("current_setpoint", "MWIR", parent=self),
                    RWCommand("voltage_setpoint", "MWVR", parent=self),
                    RWCommand("current_ramp", "MSRI", parent=self),
                    RWCommand("voltage_ramp", "MSRV", parent=self),
                    ROCommand("power", "MRW", parent=self, close=0.01),
                    ROCommand("leakage_current", "MGC", parent=self),
                    ROCommand("temperature", "MRT", parent=self, close=0.15),
                    RWCommand("password", "PASSWORD", parent=self)]:
            self.__setattr__("_%s" % (obj.name), obj)
            self._commands[obj.name] = obj
            obj.addRvalueChangeCB(self.rvalue_callback)

        self._registers = {}
        for obj in [RORegister('firmware_id', 0, parent=self),
                    RORegister('ps_model', 1, parent=self),
                    RORegister('serial_number', 2, parent=self),
                    RORegister('mac', 3, parent=self),
                    RORegister('macsfp1', 4, parent=self),
                    RORegister('macsfp2', 5, parent=self),
                    # bits 6-8 reserved
                    RORegister('calibration_date', 9, parent=self),
                    # FIXME: wrongly interpreted an answer like
                    # "#MRG:9:17/11/2017 09:09" (due to the last ':')
                    RORegister('current_calibration_param_a', 10, parent=self),
                    RORegister('current_calibration_param_b', 11, parent=self),
                    RORegister('current_calibration_param_c', 12, parent=self),
                    RORegister('current_calibration_param_d', 13, parent=self),
                    RORegister('voltage_calibration_param_a', 14, parent=self),
                    RORegister('voltage_calibration_param_b', 15, parent=self),
                    RORegister('voltage_calibration_param_c', 16, parent=self),
                    RORegister('voltage_calibration_param_d', 17, parent=self),
                    RORegister('dc_link_calib_a', 18, parent=self),
                    RORegister('dc_link_calib_b', 19, parent=self),
                    RORegister('ac_link_calib_a', 20, parent=self),
                    RORegister('ac_link_calib_b', 21, parent=self),
                    RORegister('current_leakage_calib_a', 22, parent=self),
                    RORegister('current_leakage_calib_b', 23, parent=self),
                    # bits 24-29 reserved
                    RWRegister('module_id', 30, parent=self),
                    RWRegister('default_current_slew_rate', 31, parent=self),
                    RWRegister('default_voltage_slew_rate', 32, parent=self),
                    # bits 33-39 reserved
                    RWRegister('pid_i_kp_v', 40, parent=self),
                    RWRegister('pid_i_ki_v', 41, parent=self),
                    RWRegister('pid_i_kd_v', 42, parent=self),
                    RWRegister('pid_i_kp_i', 43, parent=self),
                    RWRegister('pid_i_ki_i', 44, parent=self),
                    RWRegister('pid_i_kd_i', 45, parent=self),
                    RWRegister('pid_i_upper_lim_acc_v', 46, parent=self),
                    RWRegister('pid_i_lower_lim_acc_v', 47, parent=self),
                    # bits 48-49 reserved
                    RWRegister('pid_v_kp_i', 60, parent=self),
                    RWRegister('pid_v_ki_i', 61, parent=self),
                    RWRegister('pid_v_kd_i', 62, parent=self),
                    RWRegister('pid_v_kp_v', 63, parent=self),
                    RWRegister('pid_v_ki_v', 64, parent=self),
                    RWRegister('pid_v_kd_v', 65, parent=self),
                    RWRegister('pid_v_upper_lim_acc_i', 66, parent=self),
                    RWRegister('pid_v_lower_lim_acc_i', 67, parent=self),
                    # bits 68-79 reserved
                    RORegister('max_current_setpoint', 80, parent=self),
                    RORegister('max_voltage_setpoint', 81, parent=self),
                    RORegister('max_mofset_temp', 82, parent=self),
                    RORegister('min_dclink_threshold', 83, parent=self),
                    RORegister('earth_leakage_limit', 84, parent=self),
                    # bit 85 reserved
                    RORegister('current_regulation_fault_limit', 86,
                               parent=self),
                    RORegister('voltage_regulation_fault_limit', 87,
                               parent=self),
                    RORegister('regulation_fault_intervention_time', 88,
                               parent=self),
                    # bit 89 reserved
                    RORegister('interlock_enable_mask', 90, parent=self),
                    RORegister('interlock_activation_state', 91, parent=self),
                    RORegister('interlock1_intervention_time', 92,
                               parent=self),
                    RORegister('interlock1_name', 93, parent=self),
                    RORegister('interlock2_intervention_time', 94,
                               parent=self),
                    RORegister('interlock2_name', 95, parent=self),
                    # bits 96 and 97 blocked at the time being 12/22/2015
                    # RORegister('interlock3_intervention_time', 96,
                    #            parent=self),
                    # RORegister('interlock3_name', 97, parent=self),
                    # bits 98-99 reserved
                    ]:
            self._registers[obj.name] = obj
            obj.addRvalueChangeCB(self.rvalue_callback)

        self._rvalue_change_cbs = []

        self._refresh_period = refresh
        self._exitter = Event()
        self._exitter.clear()
        self._updater = Thread(target=self._update)
        self._updater.setDaemon(True)

        self.connect()

        self._updater.start()

    @property
    def _socket_conf(self):
        return (self._ip, self._port)

    def link(self):
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.settimeout(self._timeout)
        self._buff = MAX_BUFF_SIZE_MODULE
        self._semaphore = Lock()

    def isConnected(self):
        try:
            if self._reconnectFailed or self._socket is None:
                return False
            self._socket.getpeername()
            return True
        except:
            if not self._reconnectFailed:
                self._logger.warn("Is not Connected!")
            self._socket = None
            return False

    def connect(self):
        try:
            if self._socket is None:
                self.link()
            self._socket.connect(self._socket_conf)
            self._logger.info("connect() succeeded")
            self._reconnectFailed = False
            return True
        except Exception as e:
            if not self._reconnectFailed:
                self._logger.error("Exception doing connect(): %s" % (e))
            # raise SystemError("Not possible to connect to %s" % (ip))
            self._reconnectFailed = True
            return False

    def _send_msg(self, cmd):
        """
        Method to send a command TO the HW
        :param cmd: Command to send
        """
        try:
            self._socket.sendto(cmd, self._socket_conf)
        except socket.timeout as e:
            msg = 'Program stopped by socket sending timeout'
            self._logger.warn("_send_msg(%s) Exception: %s" % (cmd, msg))
            raise FastPSError(ERR_FASTPS_COMM_TIMEOUT)

    def _receive_msg(self):
        """
        Method to receive a command FROM the HW
        :return: String with the received answer
        """
        try:
            result, socket_rsv = self._socket.recvfrom(self._buff)
        except socket.timeout as e:
            msg = 'Program stopped by socket receiving timeout'
            self._logger.warn("_receive_msg() Exception: %s" % (msg))
            raise FastPSError(ERR_FASTPS_COMM_TIMEOUT)
        return result

    def __getattr__(self, name):
        """
        Refactored method to extract the dynamic attributes value or to call
        the upper method
        :param name: of the attribute to get
        :return: value: value of the attr
        """
        if self._registers is not None and name in self._registers.keys():
            return self.read_register(name)
        elif self._commands is not None and name in self._commands.keys():
            super(FastPS, self).__getattr__("_update_%s" % (name))()
            return self._commands[name].rvalue
        else:
            try:
                return super(FastPS, self).__getattr__(name)
            except Exception as e:
                # traceback.print_stack()
                raise KeyError("%s not present" % (name))

    def __setattr__(self, name, value):
        """
        Refactored method to set the dynamic attributes value or to call
        the upper method passing the setting value
        :param name: of the attribute to set
        :param value: value to set
        """
        if self._registers is not None and name in self._registers.keys():
            self.write_register(name, value)
        elif self._commands is not None and name in self._commands.keys():
            self._commands[name].wvalue = value
            if name in ['current_setpoint', 'voltage_setpoint',
                        'current_ramp', 'voltage_ramp'] and \
                    self.state in ['OFF']:
                self._logger.info(
                    "{name} waiting to apply {value} when OFF state".format(
                        name=name, value=value))
            else:
                self.write_cmd(self._commands[name].command, value)
        else:
            try:
                super(FastPS, self).__setattr__(name, value)
            except Exception as e:
                # traceback.print_stack()
                raise KeyError("%s not present" % (name))

    def read_register(self, name):
        reading = self.command(self._registers[name].command)
        try:
            self._registers[name].rvalue = reading.rsplit(':', 1)[1]
        except Exception as e:
            self._logger.error("Couldn't interpret {value}"
                               "".format(value=reading))
            raise e
        else:
            return self._registers[name].rvalue

    def write_register(self, name, value):
        obj = self._registers[name]
        if isinstance(obj, RWRegister):
            # register = obj.register
            if obj.wvalue != value:
                obj.wvalue = value
                self.write_cmd(obj.command, value)
        else:
            raise FastPSError(ERR_FASTPS_BAD_PARAMS)

    # ------------------------------------------------------------------
    #   Commands
    # ------------------------------------------------------------------
    def command(self, cmd):
        """
        Method to send command to Fast PS. It parses the AK or NAK return.
        :param cmd: Command to send to HW
        :return: HW answer
        """
        with self._semaphore:
            self._send_msg("%s\r\n" % (cmd))
            self._logger.debug("send command %r" % (cmd))
            raw_value = self._receive_msg().strip('\r\n')
            self._logger.debug("recv answer %r" % raw_value)
            try:
                header, answer = raw_value.split(':', 1)
            except:
                header = raw_value
                answer = None
            if header == '#NAK':
                raise FastPSError(answer)
            elif header != '#AK':
                return answer

    def read_cmd(self, cmd):
        """
        Method to build the get command and to query the Fast PS
        :param cmd: Command to execute
        :return: HW answer
        """
        return self.command(cmd + ':?')

    def write_cmd(self, cmd,  *values):
        """
        Method to build the set command with its parameters for the Fast PS.
        :param cmd: Command
        :param values: parameters
        :return: None (AK and NAK treated on command method)
        """
        if not values:
            raise (FastPSError(ERR_FASTPS_NO_PARAMS))
        cmd = str(cmd)
        for parameter in values:
            cmd = cmd + ':' + str(parameter)
        self.command(cmd)

    # ------------------------------------------------------------------
    #   Module StatusFastPS
    # ------------------------------------------------------------------

    def on(self):
        """
        Turn ON the module
        :return: None
        """
        try:
            self.command('MON\r')
        except Exception as e:
            raise e
        else:
            self._apply2hw()

    def off(self):
        """
        Turn de module OFF
        :return: None
        """
        self.command('MOFF\r')

    def reset(self):
        """
        Reset the module status register
        :return: None
        """
        self.command('MRESET\r')

    def status(self):
        """
        Read module internal status register and generates an StatusFastPS
        object passing the value as argument
        :return: instance of StatusFastPS class generated with the actual
        status register of the HW
        """
        try:
            state_str = self.command('MST\r')

            if state_str.count('\r') > 0:
                state_str = state_str.split('\r')[0]
            status_reg = int(state_str, 16)
        except Exception as e:
            self._logger.error("Couldn't interpret the received state "
                               "register: {exception}".format(exception=e))
            raise e
        else:
            # FIXME: report if state changes
            self._status.update(status_reg)

    @property
    def state(self):
        self.status()
        return self._status.state

    # ------------------------------------------------------------------
    #   Module Modes
    # ------------------------------------------------------------------
    @property
    def loop(self):
        """
        Query for the power supply loop mode (C.C. or C.V.)
        :return: Loop mode.

        """
        # TODO: To fix wrong answer from the HW after the firmware upgrade
        value = self.read_cmd('LOOP')
        if '\x00' in value:
            value = value[0]
        if self._loop != value:
            self._loop = value
        return self._outputmode[self._loop]

    @loop.setter
    def loop(self, mode):
        """
        Set the power module loop mode.
        Modes: C.C. constant current or C.V. constant voltage
        :param mode: mode to set.
        :return: None
        """
        if mode == self._outputmode.CURRENT:
            mode = 'I'
        elif mode == self._outputmode.VOLTAGE:
            mode = 'V'
        else:
            raise (FastPSError(ERR_FASTPS_WRONG_PARAMS))
        self.write_cmd('LOOP', mode)
        self._update_loop()

    # ------------------------------------------------------------------
    #   Module Getters and Setters
    # ------------------------------------------------------------------
    @property
    def current(self):
        """
        Read current value ('I')
        :return: Output read current.
        """
        self._update_current()
        return self._current.rvalue

    @property
    def voltage(self):
        """
        Read voltage value ('V')
        :return: Output read voltage.
        """
        self._update_voltage()
        return self._voltage.rvalue

    @property
    def current_setpoint(self):
        """
        Read last set current  value set ('I')
        :return: Set current read.
        """
        self._update_current_setpoint()
        return self._current_setpoint.rvalue

    @current_setpoint.setter
    def current_setpoint(self, value=0):
        """
        Set the new current setpoint
        :param value: setpoint
        :return: None
        """
        if self._current_setpoint.wvalue != value:
            self._current_setpoint.wvalue = value
            self._apply2hw_current_setpoint()

    @property
    def current_setpoint_wvalue(self):
        return self._current_setpoint.wvalue

    @property
    def voltage_setpoint(self):
        """
        Read last set voltage value set ('V')
        :return: Set voltage value.
        """
        self._update_voltage_setpoint()
        return self._voltage_setpoint.rvalue

    @voltage_setpoint.setter
    def voltage_setpoint(self, value=0):
        """
        Set the new voltage setpoint
        :param value: setpoint
        :return: None
        """
        if self._voltage_setpoint.wvalue != value:
            self._voltage_setpoint = value
            self._apply2hw_voltage_setpoint()

    @property
    def voltage_setpoint_wvalue(self):
        return self._voltage_setpoint.wvalue

    @property
    def current_ramp(self):
        """
        Query for the last accepted current ramp setpoint
        :return: Last current setpoint.
        """
        self._update_current_ramp()
        return self._current_ramp.rvalue

    @current_ramp.setter
    def current_ramp(self, value=0):
        """
        Set the new current setpoint with a ramp
        :param value: setpoint
        :return: None
        """
        if self._current_ramp.wvalue != value:
            self._current_ramp.wvalue = value
            self._apply2hw_current_ramp()

    @property
    def current_ramp_wvalue(self):
        return self._current_ramp.wvalue


    @property
    def voltage_ramp(self):
        """
        Query for the last accepted voltage ramp setpoint
        :return: Last voltage setpoint.
        """
        self._update_voltage_ramp()
        return self._voltage_ramp.rvalue

    @voltage_ramp.setter
    def voltage_ramp(self, value=0):
        """
        Set the new voltage setpoint with a ramp
        :param value: setpoint
        :return: None
        """
        if self._voltage_ramp.wvalue != value:
            self._voltage_ramp.wvalue = value
            self._apply2hw_voltage_ramp()

    @property
    def voltage_ramp_wvalue(self):
        return self._voltage_ramp.wvalue


    @property
    def power(self):
        """
        Read estimated active output power value [W]
        :return:  Active output power value.
        """
        self._update_power()
        return self._power.rvalue

    @property
    def leakage_current(self):
        """
        Read leakage current value [A]
        :return: Leakage current value.
        """
        self._update_leakage_current()
        return self._leakage_current.rvalue

    # ------------------------------------------------------------------
    #   Module Control
    # ------------------------------------------------------------------
    @property
    def temperature(self):
        """
        Read MOSFET Heatsink Temperature [degC]
        :return: Temperature value.

        """
        self._update_temperature()
        return self._temperature.rvalue

    # ------------------------------------------------------------------
    #   Module information and configuration
    # ------------------------------------------------------------------
    @property
    def firmware_vers(self):
        """
        Query the module model and installed firmware versions
        :return: ASCII indicating the module model and firmware version.
        """
        return self.command('VER').split(':')[1]

    @property
    def module_id(self):
        """
        Read module identification
        :return: Module identification (ASCII).
        """
        return self.command('MRID')

    @property
    def password(self):
        """
        Query for the actual user privileges
        :return: User privileges.
        """
        self._update_password()
        return self._password.rvalue

    @password.setter
    def password(self, password_word):
        """
        Locks or unlocks the protected parameter fields
        :param password_word
        :return: None
        """
        if self._password.wvalue != password_word:
            self._password.wvalue = password_word
            self.write_cmd(self._password.command, password_word)

    def save_parameter(self):
        """
        Write to none volatile memory the actual parameters
        :return: None
        """
        self.command('MSAVE\r')

    def get_parameter_names(self):
        """
        Getter of the parameters keys.
        :return: Parameters name
        """
        return self._registers.keys()

    def is_parameter_writable(self, name):
        """
        Method to check if a specific parameter is writable or it is protected
        :return: bool
        """
        return isinstance(self._registers[name], RWRegister)

    def addRvalueChangeCB(self, cb):
        if self._rvalue_change_cbs.count(cb) > 0:
            raise AssertionError("Already exist")
        if self._rvalue_change_cbs.count(None) > 0:
            idx = self._rvalue_change_cbs.index(None)
            self._rvalue_change_cbs[idx] = cb
        else:
            self._rvalue_change_cbs.append(cb)
            idx = self._rvalue_change_cbs.index(cb)
        self._logger.debug("Added %d callback %s to the FastPS"
                           % (idx, cb.__name__))
        return idx

    def removeRvalueChangeCB(self, idx):
        if len(self._rvalue_change_cbs) >= idx:
            if self._rvalue_change_cbs[idx] is not None:
                cb = self._rvalue_change_cbs[idx]
                self._rvalue_change_cbs[idx] = None
                self._logger.debug("Removed %d callback %s to the FastPS"
                                   % (idx, cb.__name__))
                return True
        return False

    def rvalue_callback(self, obj):
        self._logger.debug("received a callback from %s: "
                           "rvalue %s, timestamp %s, quality %s"
                           % (obj.name, obj.rvalue, obj.timestamp,
                              obj.quality))
        for cb in self._rvalue_change_cbs:
            if cb is not None:
                self._logger.debug("FastPS callback %s" % (cb.__name__))
                cb(obj.name, obj.rvalue, obj.timestamp, obj.quality)

    def _update(self):
        while not self._exitter.isSet():
            t0 = time()
            if self.isConnected():
                try:
                    state = self.state
                except:
                    self._logger.error("State not present, connection lost")
                else:
                    if state != StatusMode.FAULT:
                        self._update_commands()
                        self._update_registers()
            tused = time() - t0
            tdiff = self._refresh_period - tused
            if not self._reconnectFailed:
                self._logger.debug("updater thread has take %g seconds "
                                   "(sleep %g)" % (tused, tdiff))
            self._exitter.wait(tdiff)
        self._logger.info("Exiting updater thread")

    def _update_commands(self):
        self._update_loop()
        self._update_current()
        self._update_voltage()
        self._update_current_setpoint()
        self._update_voltage_setpoint()
        self._update_current_ramp()
        self._update_voltage_ramp()
        self._update_power()
        self._update_leakage_current()
        self._update_temperature()

    def _update_registers(self):
        for name in self._registers.keys():
            self.read_register(name)

    def _update_loop(self):
        self.loop

    def _update_current(self):
        self._current.rvalue = float(self.command(self._current.command))

    def _update_voltage(self):
        self._voltage.rvalue = float(self.command(self._voltage.command))

    def _update_current_setpoint(self):
        try:
            if self._status.state == StatusMode.ON and self._loop is 'I':
                self._current_setpoint.rvalue = float(
                    self.read_cmd(self._current_setpoint.command))
        except Exception as e:
            self._logger.warning(
                "update current setpoint exception: {e}".format(e=e))

    def _update_voltage_setpoint(self):
        try:
            if self._status.state == StatusMode.ON and self._loop is 'V':
                self._voltage_setpoint.rvalue = float(
                    self.read_cmd(self._voltage_setpoint.command))
        except Exception as e:
            self._logger.warning(
                "update voltage setpoint exception: {e}".format(e=e))

    def _update_current_ramp(self):
        try:
            if self._status.state == StatusMode.ON and self._loop is 'I':
                self._current_ramp.rvalue = float(
                    self.read_cmd(self._current_ramp.command))
        except Exception as e:
            self._logger.warning(
                "update current ramp exception: {e}".format(e=e))

    def _update_voltage_ramp(self):
        try:
            if self._status.state == StatusMode.ON and self._loop is 'V':
                self._voltage_ramp.rvalue = float(
                    self.read_cmd(self._voltage_ramp.command))
        except Exception as e:
            self._logger.warning(
                "update voltage ramp exception: {e}".format(e=e))

    def _update_power(self):
        self._power.rvalue = float(self.read_cmd(self._power.command))

    def _update_leakage_current(self):
        self._leakage_current.rvalue = float(
            self.command(self._leakage_current.command))

    def _update_temperature(self):
        self._temperature.rvalue = float(
            self.command(self._temperature.command))

    def _update_password(self):
        self._password.rvalue = self.read_cmd(self._password.command)

    def _apply2hw(self):
        self._apply2hw_current_setpoint()
        self._apply2hw_voltage_setpoint()
        self._apply2hw_current_ramp()
        self._apply2hw_voltage_ramp()

    def _apply2hw_current_setpoint(self):
        self._apply2hw_generic(self._current_setpoint, "Current setpoint",
                               "Voltage")

    def _apply2hw_voltage_setpoint(self):
        self._apply2hw_generic(self._voltage_setpoint, "Voltage setpoint",
                               "Current")

    def _apply2hw_current_ramp(self):
        self._apply2hw_generic(self._current_ramp, "Current ramp", "Voltage")

    def _apply2hw_voltage_ramp(self):
        self._apply2hw_generic(self._voltage_ramp, "Voltage ramp", "Current")

    def _apply2hw_generic(self, obj, name, loop):
        if obj.rvalue == obj.wvalue:
            self._logger.warning(
                "{name} rewrite of {value}".format(
                    name=name, value=obj.wvalue))
        if self.state in ['OFF']:
            self._logger.info(
                "{name} waiting to apply {value} when OFF state".format(
                    name=name, value=obj.wvalue))
        elif self.loop in loop:
            self._logger.debug(
                "{name} cannot be written while in {loop} regulation".format(
                    name=name, loop=loop))
        else:
            value = obj.wvalue if not isnan(obj.wvalue) else obj.rvalue
            if isnan(value):
                self._logger.debug(
                    "Nothing to apply for {name}".format(name=name))
            else:
                self._logger.info(
                    "apply stored {name} {value}".format(
                        name=name, value=obj.wvalue))
                self.write_cmd(obj.command, obj.wvalue)

    def __del__(self):
        """
        Method to close the socket when deleting the object.
        """
        self._exitter.set()
        self._socket.close()
        self._logger.info("Close socket communications")
        # Truncate the log file.
        with open(self._logfile, 'w'):
            pass
