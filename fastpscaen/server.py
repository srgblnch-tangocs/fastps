# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

try:
    from tango import Util, DevFailed
except Exception:
    from PyTango import Util, DevFailed
import sys
from .device import FastPSDevice, FastPSClass

__author__ = "Roger Pastor Ortiz/Sergi Blanch-Torne"
__copyright__ = "Copyright 2015-2018, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

SERVER_NAME = 'FastPS'


def run(args=None):
    __version__ = '1.0.12-alpha'
    print('Device Server Version: %s' % __version__)

    try:
        if not args:
            args = sys.argv[1:]
            args = [SERVER_NAME] + list(args)
        util = Util(args)
        util.add_class(FastPSClass, FastPSDevice)
        u = Util.instance()
        u.server_init()
        u.server_run()

    except DevFailed, e:
        print('-------> Received a DevFailed exception:', e)
    except Exception, e:
        print('-------> An unforeseen exception occurred....', e)


if __name__ == '__main__':
    run()
