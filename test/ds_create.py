# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>
#
# ##### END GPL LICENSE BLOCK #####

try:
    import CAENels
except:
    CAENels = None
from fandango import Astor
import getopt
try:
    import tango
except:
    import PyTango as tango
from socket import gethostname
import sys
from time import sleep


__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2018, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


class Inspector(object):
    _devs = None

    def __init__(self):
        super(Inspector, self).__init__()
        self._devs = [tango.DeviceProxy(
            'sr{sector:02d}/pc/trim-{sequence:02d}'.format(
            sector=sector, sequence=sequence))
            for sector in range(1, 17) for sequence in range(1, 3)]
        # ['sr{sector:02d}/pc/trim-{sequence:02d}'.format(
        #     sector=sector, sequence=sequence)
        #     for sector in range(1, 17) for sequence in range(1, 3)]




class TrimCoils(object):
    _devServer = 'FastPS'
    _devClass = 'FastPSDevice'

    _pattern = {'devName': 'sr{sector:02d}/pc/trim-{sequence:02d}',
                'hostName': 'dpcsr{sector:02d}t{sequence:02d}',
                'Port': 10001,
                'instance': 'sr{sector:02d}',
                'machine': 'cpc{sector:02d}01',
                }

    _nSectors = None
    _nPerSector = None

    _simulators = None

    def __init__(self, nSectors=16, nPerSector=2):
        super(TrimCoils, self).__init__()
        self._nSectors = nSectors
        self._nPerSector = nPerSector
        self.__db = tango.Database()
        self._simulators = []

    def generator(self, dry=False, machine=None, singleInstance=False,
                  logpath=None, simulation=False):
        for sector in range(1, self._nSectors+1):
            if not singleInstance:
                instance = self._pattern['instance'].format(sector=sector)
            if simulation:
                devHost = gethostname()
            elif machine is None:
                devHost = self._pattern['machine'].format(sector=sector)
            else:
                devHost = machine
            for sequence in range(1, self._nPerSector+1):
                if singleInstance:
                    pattern = self._pattern['instance']+"_trim{sequence:02d}"
                    instance = pattern.format(
                        sector=sector, sequence=sequence)
                devName = self._pattern['devName'].format(
                    sector=sector, sequence=sequence)
                if simulation:
                    hostName = '127.0.0.1'
                    port = self._pattern['Port']+len(self._simulators)+1
                    simulator = CAENels.FastPS_PC(port=port)
                    simulator.open()
                    self._simulators.append(simulator)
                else:
                    hostName = self._pattern['hostName'].format(
                        sector=sector, sequence=sequence)
                    port = None
                self.__createDevice(self._devServer, instance,
                                    self._devClass, devName, dry)
                if logpath is None:
                    logpath = '/var/tmp/ds.log'
                if singleInstance:
                    logfileName = '{logpath}/FastPS_{instance}.log' \
                                  ''.format(logpath=logpath,
                                            instance=instance)
                else:
                    logfileName = '{logpath}/FastPS_{instance}' \
                                  '_trim{sequence:02d}.log' \
                                  ''.format(logpath=logpath,
                                            instance=instance,
                                            sequence=sequence)
                self.__setProperties(devName,
                                     {'HostIP': hostName,
                                      'Logfile': logfileName,
                                      'Port': port,
                                     }, dry)
                if singleInstance:
                    self.__setStarter(instance, devHost, dry)
            if not singleInstance:
                self.__setStarter(instance, devHost, dry)
        if simulation:
            while True:
                try:
                    answer = raw_input("\nMay you like to stop "
                                       "this simulation? [y/N]\n")
                    if answer.lower() in ['y', 'yes']:
                        break
                except SyntaxError:
                    pass
            self.delete(dry, singleInstance)

    def delete(self, dry=False, singleInstance=False):
        for sector in range(1, self._nSectors+1):
            if not singleInstance:
                instance = self._pattern['instance'].format(sector=sector)
                self.__deleteDevice(self._devServer, instance, dry)
            else:
                for sequence in range(1, self._nPerSector+1):
                    pattern = self._pattern['instance'] + "_trim{sequence:02d}"
                    instance = pattern.format(
                        sector=sector, sequence=sequence)
                    self.__deleteDevice(self._devServer, instance, dry)

    def start(self, dry=False, singleInstance=False):
        instances = self.__prepare(dry, singleInstance)
        for instance in instances:
            serverInstance = "{server}/{instance}".format(
                server=self._devServer, instance=instance)
            if not dry:
                self.__startInstance(serverInstance)

    def stop(self, dry=False, singleInstance=False):
        instances = self.__prepare(dry, singleInstance)
        for instance in instances:
            serverInstance = "{server}/{instance}".format(
                server=self._devServer, instance=instance)
            if not dry:
                self.__stopInstance(serverInstance)

    def restart(self, dry=False, singleInstance=False):
        instances = self.__prepare(dry, singleInstance)
        print("Going to restart {n:d} device instances"
              "".format(n=len(instances)))
        for instance in instances:
            serverInstance = "{server}/{instance}".format(
                server=self._devServer, instance=instance)
            if not dry:
                self.__stopInstance(serverInstance)
        print("Wait until they all are down")
        sleep(3)  # FIXME: check, not wait.
        for instance in instances:
            serverInstance = "{server}/{instance}".format(
                server=self._devServer, instance=instance)
            if not dry:
                self.__startInstance(serverInstance)

    def __prepare(self, dry=False, singleInstance=False):
        instances = []
        for sector in range(1, self._nSectors+1):
            if not singleInstance:
                instance = self._pattern['instance'].format(sector=sector)
                # serverInstance = "{server}/{instance}".format(
                #     server=self._devServer, instance=instance)
                instances.append(instance)
                # self.__restartDevice(serverInstance)
            else:
                pattern = self._pattern['instance'].format(sector=sector)
                for sequence in range(1, self._nPerSector+1):
                    instance = \
                        pattern+"_trim{sequence:02d}".format(sequence=sequence)
                    # serverInstance = "{server}/{instance}".format(
                    #     server=self._devServer, instance=instance)
                    instances.append(instance)
                    # self.__restartDevice(serverInstance)
        return instances

    def __startInstance(self, serverInstance):
        astor = Astor()
        print("\tStart {instance}".format(instance=serverInstance))
        astor.start_servers([serverInstance])

    def __stopInstance(self, serverInstance):
        astor = Astor()
        print("\tStop {instance}".format(instance=serverInstance))
        astor.stop_servers([serverInstance])

    def __createDevice(self, dev_server, dev_instance, dev_class, dev_name,
                       dry=False):
        try:
            print("\nCreating '%s' device (server:'%s',instance:'%s',"
                  "class:'%s')..." % (dev_name, dev_server, dev_instance,
                                      dev_class))
            if not dry:
                di = tango.DbDevInfo()
                di.name, di._class, di.server = dev_name, dev_class, \
                    dev_server + '/' + dev_instance
                self.__db.add_device(di)
        except Exception as e:
            print("Cannot create '%s' device due to an exception:\n%s\n"
                  % (dev_name, e))

    def __deleteDevice(self, dev_server, dev_instance, dry=False):
        try:
            print("Deleting '%s' instance of the '%s' server"
                  % (dev_instance, dev_server))
            if not dry:
                serverInstance = "{server}/{instance}".format(
                    server=dev_server, instance=dev_instance)
                self.__stopInstance(serverInstance)
                self.__db.delete_server(dev_server + '/' + dev_instance)
        except Exception as e:
            print("Cannot delete '%s' server due to an exception:\n%s\n"
                  % (dev_server, e))

    def __setProperties(self, dev_name, dev_prop, dry=False):
        print("Properties for the device '%s'" % (dev_name))
        for k in dev_prop.keys():
            try:
                value = dev_prop[k]
                if value is not None:
                    print("Settining up the property '%s' with value '%s'"
                          % (k, value))
                    if not dry:
                        self.__db.put_device_property(dev_name, {k: value})
            except Exception as e:
                print("Cannot set the property '%s' for the '%s' device due "
                      "to an exception:\n%s\n" % (k, dev_name, e))

    def __setStarter(self, instance, devHost, dry=False):
        try:
            astor = Astor()
            runLevel = 2
            serverInstance = "{server}/{instance}".format(
                server=self._devServer, instance=instance)
            print("Include in the {host} starter the instance {instance} "
                  "in run level {level}".format(
                host=devHost, instance=serverInstance, level=runLevel))
            if not dry:
                astor.set_server_level(serverInstance, devHost, runLevel)
                astor.start_servers(serverInstance, devHost, wait=100)
        except Exception as e:
            print("Cannot setup the starter for instance {instance} "
                  "at host {host}".format(instance=instance, host=devHost))


def help():
    print("")
    print("usage {argv} [--dry-run] [--delete|start|stop|restart] "
          "[--machine=hostname] [--logpath=path] [--simulation]"
          "".format(argv=sys.argv[0]))
    print("Options and arguments:")
    print("-h, -?, --help      : outputs this lines")
    print("--dry-run           : do everything except to modify the tango-db")
    print("--delete            : used to remove those devices")
    print("--start             : used to start those devices")
    print("--stop              : used to stop those devices")
    print("--restart           : used to restart those devices")
    print("--machine=hostname  : specify that all the instances will run in "
          "the specified machine instead of the pattern")
    print("--single-instance   : By default each device will live in a single "
          "device server. With this option all the devices in the same sector "
          " will live in the same device server.")
    print("--logpath=path      : descriptive path tho were the logs will be "
          "written.")
    print("--simulation        : Create simulation objects instead of connect "
          "to real hardware (everything in localhost). The script will stay "
          "alive until user kills and those objects will be destroyed")
    print("")


def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:],
                                   "h?", ['help', 'dry-run', 'delete',
                                         'start', 'stop', 'restart',
                                         'machine=', 'single-instance',
                                         'logpath=', 'simulation'])
    except getopt.GetoptError as e:
        print str(e)
        sys.exit()
    dry = False
    machine = None
    delete = False
    start, stop, restart = False, False, False
    singleInstance = False
    logpath = None
    simulation = False
    for opt, arg in opts:
        if opt in ("-h", "-?", "--help"):
            help()
            sys.exit()
        elif opt in ('--dry-run'):
            dry = True
        elif opt in ("--delete"):
            delete = True
        elif opt in ("--start"):
            start = True
        elif opt in ("--stop"):
            stop = True
        elif opt in ("--restart"):
            restart = True
        elif opt in ("--machine"):
            machine = arg
        elif opt in ("--single-instance"):
            singleInstance = True
        elif opt in ("--logpath"):
            logpath = arg
        elif opt in ("--simulation"):
            if CAENels is None:
                print("\nThe simulation module CAENels is available. "
                      "Impossible to simulate. Abort!\n")
                sys.exit()
            simulation = True
        else:
            help()
            sys.exit()
    builder = TrimCoils()
    if delete:
        builder.delete(dry, singleInstance)
    elif start:
        builder.start(dry, singleInstance)
    elif stop:
        builder.stop(dry, singleInstance)
    elif restart:
        builder.restart(dry, singleInstance)
    else:
        builder.generator(dry, machine, singleInstance, logpath, simulation)


if __name__ == "__main__":
    main()
