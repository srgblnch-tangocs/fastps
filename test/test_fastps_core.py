# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


from fastpscaen import FastPS, TCP_PORT, StatusMode
import unittest
from unittest import TestCase, TestSuite, TextTestRunner
import random
# import time

__author__ = "Roger Pastor Ortiz/Sergi Blanch-Torne"
__copyright__ = "Copyright 2015-2018, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


class FastPSCAENCoreTest(TestCase):
    def __init__(self, host, port, methodName='runTest'):
        super(FastPSCAENCoreTest, self).__init__(methodName)
        self.fastps = FastPS(host, port)

    def runTest(self, result=None):
        raise NotImplementedError('You must implement it')

    def test_stop(self):
        self.fastps.stop()
        msg = 'FAST-PS not stopped properly'
        self.assertEqual(self.fastps.status().state, StatusMode.OFF, msg)

    def test_set_loop_mode(self):
        self.fastps.loop = self.fastps.outputmode.CURRENT
        msg = 'Current mode not set correctly on loop'
        self.assertEqual(self.fastps.loop, self.fastps.outputmode.CURRENT, msg)
        msg = 'Current mode not set correctly on parameter'
        self.assertEqual(self.fastps.status().regulationmode,
                         self.fastps.outputmode.CURRENT, msg)
        self.fastps.loop = self.fastps.outputmode.VOLTAGE
        msg = 'Current mode not set correctly on loop'
        self.assertEqual(self.fastps.loop, self.fastps.outputmode.VOLTAGE, msg)
        msg = 'Current mode not set correctly on parameter'
        self.assertEqual(self.fastps.status().regulationmode,
                         self.fastps.outputmode.VOLTAGE, msg)

    # TODO: Rename 'self.fastps.outputmode' and classes of the kind
    # def test_start(self):
    #     self.fastps.start()
    #     msg = 'FAST-PS not started properly'
    #     self.assertEqual(self.fastps.status().state, StatusMode.ON, msg)
    #
    # def test_current_setpoint(self):
    #     current = random.uniform(-0.005,0.005)
    #     self.fastps.loop(self.fastps.outputmode.CURRENT)
    #     self.fastps.current_setpoint = current
    #     msg = 'FAST-PS current_setpoint not set properly'
    #     self.assertEqual(self.fastps.current_setpoint, current, msg)
    #
    # def test_voltage_setpoint(self):
    #     voltage = random.uniform(-0.005,0.005)
    #     self.fastps.loop(self.fastps.outputmode.VOLTAGE)
    #     self.fastps.voltage_setpoint = voltage
    #     msg = 'FAST-PS voltage_setpoint not set properly'
    #     self.assertEqual(self.fastps.voltage_setpoint, voltage, msg)
    #
    # def test_current_ramp(self):
    #     ramp = random.uniform(-0.005,0.005)
    #     self.fastps.loop(self.fastps.outputmode.CURRENT)
    #     self.fastps.current_ramp = ramp
    #     msg = 'FAST-PS current_setpoint not set properly'
    #     self.assertEqual(self.fastps.current_ramp, ramp, msg)
    #
    # def test_voltage_ramp(self):
    #     ramp = random.uniform(-0.005,0.005)
    #     self.fastps.loop(self.fastps.outputmode.VOLTAGE)
    #     self.fastps.voltage_ramp = ramp
    #     msg = 'FAST-PS voltage_setpoint not set properly'
    #     self.assertEqual(self.fastps.voltage_ramp, ramp, msg)
    #
    # def test_current_slew(self):
    #     slew = random.uniform(-0.005,0.005)
    #     self.fastps.loop(OutputMode.CURRENT)
    #     self.fastps.current_slew = slew
    #     msg = 'FAST-PS current_setpoint not set properly'
    #     self.assertEqual(self.fastps.current_slew, slew, msg)
    #
    # def test_voltage_slew(self):
    #     slew = random.uniform(-0.005,0.005)
    #     self.fastps.loop(OutputMode.VOLTAGE)
    #     self.fastps.voltage_slew = slew
    #     msg = 'FAST-PS voltage_setpoint not set properly'
    #     self.assertEqual(self.fastps.voltage_slew, slew, msg)
    #
    # def test_reads(self):
    #     msg = 'FAST-PS temperature not read properly'
    #     self.assertTrue(self.fastps.temperature > 0, msg)
    #     msg = 'FAST-PS current not read properly. Float not returned'
    #     self.assertTrue(type(self.fastps.current) == float, msg)
    #     msg = 'FAST-PS voltage not read properly. Float not returned'
    #     self.assertTrue(type(self.fastps.voltage) == float, msg)
    #     msg = 'FAST-PS power not read properly. Float not returned'
    #     self.assertTrue(type(self.fastps.power) == float, msg)
    #     msg = 'FAST-PS leackage_current not read properly. ' \
    #           'Float not returned'
    #     self.assertTrue(type(self.fastps.leakage_current) == float, msg)
    #
    # @unittest.skip('different param values.')
    # def test_params(self):
    #     msg = 'FAST-PS firmware not read properly'
    #     self.assertTrue(self.fastps.firmware_vers ==
    #                     self.fastps.firmware_version, msg)
    #     msg = 'FAST-PS id not read properly'
    #     self.assertTrue(self.fastps.module_id ==
    #                     self.fastps.ps_model, msg)

    def test_restart(self):
        self.fastps.restart()
        msg = 'FAST-PS not restarted properly'
        self.assertEqual(self.fastps.status().state, StatusMode.ON, msg)


if __name__ == '__main__':
    import argparse as ap

    port = TCP_PORT
    parser = ap.ArgumentParser(description='Unittest for FastPS-CAENels core.')
    parser.add_argument('host', type=str, nargs=1, help='FastPS IP')

    args = parser.parse_args()

    host = args.host[0]

    FastPSTestSuite = TestSuite()
    FastPSTestSuite.addTest(FastPSCAENCoreTest(host, port,
                                               'test_set_loop_mode'))
    FastPSTestSuite.addTest(FastPSCAENCoreTest(host, port, 'test_start'))
    FastPSTestSuite.addTest(FastPSCAENCoreTest(host, port, 'test_stop'))
    FastPSTestSuite.addTest(FastPSCAENCoreTest(host, port,
                                               'test_current_setpoint'))
    FastPSTestSuite.addTest(FastPSCAENCoreTest(host, port,
                                               'test_voltage_setpoint'))
    FastPSTestSuite.addTest(FastPSCAENCoreTest(host, port,
                                               'test_current_ramp'))
    FastPSTestSuite.addTest(FastPSCAENCoreTest(host, port,
                                               'test_voltage_ramp'))
    FastPSTestSuite.addTest(FastPSCAENCoreTest(host, port,
                                               'test_current_slew'))
    FastPSTestSuite.addTest(FastPSCAENCoreTest(host, port,
                                               'test_voltage_slew'))
    FastPSTestSuite.addTest(FastPSCAENCoreTest(host, port, 'test_reads'))
    FastPSTestSuite.addTest(FastPSCAENCoreTest(host, port, 'test_params'))

    runner = TextTestRunner()
    result = runner.run(FastPSTestSuite)

    errors = len(result.errors)
    failures = len(result.failures)

    print('\n' * 2, '=' * 80)
    print('Results ')
    print('Error: ', errors)
    print('Failures: ', failures)
